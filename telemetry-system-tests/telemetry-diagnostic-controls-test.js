describe('Telemetry System', function () {

	describe('TelemetryDiagnosticControls', function () {

		it('CheckTransmission() should send a diagnostic message and receive a status message response', function () {

			var target = new TelemetryDiagnosticControls();
			target.checkTransmission();

			var result = target.readDiagnosticInfo();

                        expect(result).not.toBe(null);
			expect(result.split("\n").length).toBe(14);
			expect(result.split("\n")[0]).toMatch('TX rate');
		    });

	});

});
